%{
by Oskar Klos
Engineering Physics
function made in MATLAB
%}

% With this funtion we search for a value that occurs the largest amount of times within our data
function [var] = Noiselevel(data)
    [~,temp] = find(data== max(data)); 
    var = temp;
end