%{
by Oskar Klos
Engineering Physics
program made in MATLAB
%}


function [] = PeakAnalysis(fileName)

 file = dir(fileName);
 fwd  = fopen(file,'r');

 %dividing 8GB in bytes over 256 => count
 count = 33554432; %a single "chunk"
 data  = zeros(1,256);


 for i = 1:256
    values = fread(fwd,count,'unit8');
    for j = 1:count
        data(values(k)) = data(values(k))+1;
    end
    
 end

 fclose(fwd);
 noise = NoiseLevel(data); %finding the noise level
 fwd  = fopen(file,'r');

{%
 pulses = 0; %following loop counts all pulses that are not considered noise
 %by adding a number of pulses found in each chunk
 for i = 1:256
    values = fread(fwd,count,'unit8');
    pulses = pulses + CountPulses(values,noise);
    clear values;
 end
 Countpulses function is not implemented
%}

 %creating a histogram of analyzed data
 Plt = plot(data);
 xlabel('Values');
 ylabel('Number of occurances');
 title('Histogram');
 saveas(Plt , [fileName,'_histogram.pdf']);
 
 %following lines create a file and put into it additional information about analyzed data
 [~,name,~] = fileparts(fileName);%this line is used to get the name of the file
 fwd = fopen( [name, '_peak_analysis_results.dat'],'w');
 fprintf(fwd, 'Noise level: %d \n', noise);
 %fprintf(fwd, 'Number of pulses: %d \n', pulses);
 fprintf(fwd, 'Name of file: %s ', name);
 fclose(fwd);
 
 clear;
end    